(function ($) {
    'use strict'; $.fn.generateSmallChart = function () {
        var thisEle = $(this); var coin_symbol = $(this).data('coin-symbol'); var period = $(this).data('period'); var cache = $(this).data('cache'); var color = $(this).data('color'); var bgcolor = $(this).data('bg-color'); var chartfill = $(this).data('chart-fill'); var pointsSettings = $(this).data('points'); var currencyPrice = $(this).data('currency-price'); var currencySymbol = $(this).data('currency-symbol'); var points = 0; if (pointsSettings == 1) { points = 2 }
        var sparklineCon = $(this); if (cache == !0) { var historicalData = $(this).data('content'); if (historicalData !== 'undefined') { historicalData = historicalData.map(function (value) { var convertedPrice = parseFloat(value) * currencyPrice; var decimalPosition = convertedPrice >= 1 ? 2 : (convertedPrice < 0.000001 ? 8 : 6); return convertedPrice.toFixed(decimalPosition) }); thisEle.parents().find('.cmc_spinner').hide(); createChart(thisEle, historicalData, color, bgcolor, chartfill, points, currencySymbol) } else { thisEle.before('<span class="no-graphical-data">' + thisEle.data('msz') + "</span>") } } else {
            var request_data = { 'action': 'cmc_small_charts', 'symbol': coin_symbol, 'period': period }; jQuery.ajax({
                type: "post", dataType: "json", url: ajax_object.ajax_url, data: request_data, success: function (response) {
                    if (response && response.type == "success") {
                        if (response.data) { var historicalData = response.data; historicalData = historicalData.map(function (value) { var convertedPrice = parseFloat(value) * currencyPrice; var decimalPosition = convertedPrice >= 1 ? 2 : (convertedPrice < 0.000001 ? 8 : 6); return convertedPrice.toFixed(decimalPosition) }) }
                        thisEle.parents().find('.cmc_spinner').hide(); createChart(thisEle, historicalData, color, bgcolor, chartfill, points, currencySymbol)
                    } else { thisEle.before('<span class="no-graphical-data">' + thisEle.data('msz') + "</span>") }
                }
            })
        }
    }
    function createChart(element, chartData, color, bgcolor, chartfill, points, currencySymbol) {
        var ctx = element.get(0).getContext("2d")
        var cwid = document.getElementById("small-coinchart"); var color1 = color; if (color1 == "#ff0000") { var color2 = "#f5c992" } else if (color1 == "#006400") { var color2 = "#5cdfbb" } else { var color2 = color }
        var bgcolor1 = bgcolor; if (bgcolor1 == "#ff9999") { var bgcolor2 = "#f7e0c3" } else if (bgcolor1 == "#90EE90") { var bgcolor2 = "#c1ffee" } else { var bgcolor2 = bgcolor }
        var gradientStroke = ctx.createLinearGradient(0, 0, cwid.width, 0); gradientStroke.addColorStop(0, color2); gradientStroke.addColorStop(1, color1); var gradientFill = ctx.createLinearGradient(0, 0, cwid.width, 0); gradientFill.addColorStop(0, bgcolor2); gradientFill.addColorStop(1, bgcolor1); var data = { labels: chartData, datasets: [{ fill: chartfill, lineTension: 0.25, borderWidth: 1.5, pointRadius: points, data: chartData, backgroundColor: gradientFill, borderColor: gradientStroke, pointBorderColor: gradientStroke, }] }; var maxval = Math.max.apply(Math, chartData) + Math.max.apply(Math, chartData) * 1 / 100; var minval = Math.min.apply(Math, chartData) - Math.min.apply(Math, chartData) * 1 / 100; var settings = { hover: { mode: 'nearest', intersect: !0 }, maintainAspectRatio: !1, scales: { xAxes: [{ display: !1 }], yAxes: [{ display: !1, ticks: { min: minval, max: maxval, } }] }, animation: { duration: 400 }, legend: { display: !1 }, tooltips: { mode: 'index', intersect: !1, displayColors: !1, callbacks: { label: function (tooltipItem, data) { var formatTipPrice = formatPrice(parseFloat(tooltipItem.xLabel)); return currencySymbol + ' ' + formatTipPrice }, title: function (tooltipItem, data) { return !1 } } }, }; var chart = new Chart(element, { type: "line", data: data, options: settings })
    }
    function formatPrice(num) {
        if (isNaN(num)) { return '-' }
        num = (num + '').split('.'); return num[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,') + (num.length > 1 ? ('.' + num[1]) : '')
    }
})(jQuery)