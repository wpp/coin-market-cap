(function ($) {

  'use strict';

  window.mobilecheck = function () {
    var check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;

  };
  var gernateChart = function (coinId, coinData, color, chartfrom, chartto, chartzoom, pricelbl, volumelbl) {
    var mobile = window.mobilecheck();
    var marginLeft = 90;
    if (mobile) {
      marginLeft = 0;
    }
    var chart = AmCharts.makeChart('CMC-CHART-' + coinId, {
      "type": "stock", "theme": "light", "hideCredits": !0, "categoryAxesSettings": {
        "gridColor": "#eee",
        "gridAlpha": 1,
        "minPeriod": "mm"
      },
      "panelsSettings": {
        "plotAreaFillColors": "#f9f9f9",
        "plotAreaFillAlphas": 0.8,
        "marginLeft": marginLeft,
        "marginTop": 5,
        "marginBottom": 5
      },
      "valueAxesSettings": {
        "gridColor": "#eee",
        "gridAlpha": 1,
        "inside": mobile,
        "showLastLabel": true
      },
      "listeners": [{
        "event": "rendered",
        "method": function (e) {
          var preloader = document.getElementById("cmc-chart-preloader");
          preloader.parentElement.removeChild(preloader);
        }
      }]
      , "dataSets": [{
        "title": "USD", "color": color, "fieldMappings": [{
          "fromField": "value", "toField": "value"
        }
          , {
          "fromField": "volume", "toField": "volume"
        }
        ], "dataProvider": coinData, "categoryField": "date"
      }
      ], "panels": [{
        "showCategoryAxis":true, 
        "title": pricelbl, 
        "percentHeight": 70,
         "stockGraphs": [{
          "id": "g1", "valueField": "value",
           "lineThickness": 2, "bullet": "round",
            "bulletSize": 5, "fillAlphas": 0.1,
             "comparable": !0, "compareField": "value",
              "balloonText": "[[title]]:<b>[[value]]</b>",
               "compareGraphBalloonText": "[[title]]:<b>[[value]]</b>"
        }
        ], "stockLegend": {
          "periodValueTextComparing": "[[percents.value.close]]%", "periodValueTextRegular": "[[value.close]]"
        }
        , "allLabels": [{
          "x": 200, "y": 115, "text": "", "align": "center", "size": 16
        }
        ], "drawingIconsEnabled": false
      }
        , {
        "title": volumelbl, "percentHeight": 30, "stockGraphs": [{
          "valueField": "volume", "type": "column", "showBalloon": !1, "cornerRadiusTop": 2, "fillAlphas": 1
        }
        ], "stockLegend": {
          "periodValueTextRegular": "[[value.close]]"
        }
        ,
      }
      ], "chartScrollbarSettings":
		{ "graph": "g1", "usePeriod": "10mm", "position": "bottom", "backgroundColor": "#555", "graphFillColor": "#333", "graphFillAlpha": 0.8, "gridColor": "#666", "selectedBackgroundColor": "#888", "selectedGraphFillColor": "#111" }
      , "chartCursorSettings": {
        "valueBalloonsEnabled": !0, "fullWidth": !0, "cursorAlpha": 0.1, "valueLineBalloonEnabled": !0, "valueLineEnabled": !0, "valueLineAlpha": 0.5
      }
      , "periodSelector": {
        "position": "top", "periodsText": chartzoom, "fromText": chartfrom, "toText": chartto, "periods": [{
          "period": "DD", "count": 1, "label": "1D"
        }
          , {
          "period": "DD", "count": 7, "label": "7D"
        }
          , {
          "period": "MM", "selected": !0, "count": 1, "label": "1M"
        }
          , {
          "period": "MM", "count": 3, "label": "3M"
        }
          , {
          "period": "MM", "count": 6, "label": "6M"
        }
          , {
          "period": "MAX", "label": "1Y"
        }
        ]
      }
      , "export": {
        "enabled": false, "position": "top-right"
      }
    }
    )
  }
  jQuery('.cmc-chart').each(function (index) {
    var coinId = jQuery(this).data("coin-id");
    var chart_color = jQuery(this).data("chart-color");
    var coinperiod = jQuery(this).data("coin-period");
    var chartfrom = jQuery(this).data("chart-from");
    var chartto = jQuery(this).data("chart-to");
    var chartzoom = jQuery(this).data("chart-zoom");
    var pricelbl = jQuery(this).data("chart-price");
    var volumelbl = jQuery(this).data("chart-volume");
    var priceData = [];
    var volData = [];
    jQuery(this).find(".cmc-preloader").show();
    var mainThis = jQuery(this);
    var apiUrl = 'https://coincap.io';
    var price_section = jQuery(this).find(".CCP-" + coinId);
    var raw_data = $("#" + coinId + '-chart-data').html();
    var priceData = JSON.parse(raw_data);
    gernateChart(coinId, priceData, chart_color, chartfrom, chartto, chartzoom, pricelbl, volumelbl)
  }
  )
}

)(jQuery)