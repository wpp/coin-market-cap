<?php

    $coin_data='';
    $coin_name = $coin['name'];
    $c_id = $coin['id'];
    $coin_symbol =$coin['symbol'];
    $coin_icon= coin_logo_html($c_id);
    $supply=$coin['available_supply'];
    $percent_change_7d=$coin['percent_change_7d'] . '%';
    $percent_change_24h=$coin['percent_change_24h'] . '%';
    $price_index="price_".strtolower($old_currency);
    $m_index="market_cap_".strtolower($old_currency);
    $v_index="24h_volume_".strtolower($old_currency);

    
	if(isset($coin[$price_index])){
	 	 $coin_price = $coin[$price_index];
	        }else{
	        	 $coin_price = $coin['price_usd'];
	        }

	     $coin_price=format_number($coin_price);
		   if(isset($coin[$v_index])){
	 			 $volume = $coin[$v_index];
	        }else{
	        	 $volume = $coin['price_usd'];
	        }
		
			 if($enable_formatting){
        	$formatted_volume=cmc_format_coin_values($volume);
        	}else{
        	$formatted_volume=format_number($volume);	
        	}	
			 if(isset($coin[$m_index])){
	 			 $market_cap = $coin[$m_index];
		        }else{
		        	 $market_cap = $coin['market_cap_usd'];

		        }
		  $volume=format_number($volume);
		
	 	 
	 	 if($enable_formatting){
        	 $formatted_supply=cmc_format_coin_values($supply);	
        	}else{
        	 $formatted_supply=format_number($supply);
        	}
          $supply=format_number($supply);	
		 
		 if($market_cap==""){
			$formatted_market_cap="?";	 
		 }else{

		 	 if($enable_formatting){	
				$formatted_market_cap=cmc_format_coin_values($market_cap);	
			 }else{
			 	$formatted_market_cap=format_number($market_cap);
			 } 
		 }
		  $market_cap=format_number($market_cap);  
		  $currency_icon=cmc_old_cur_symbol($old_currency);
	       if($coin_symbol=="MIOTA"){
	       	 	$coinId='IOT';
	       } else{
	       	$coinId=$coin_symbol;
	       }
	      $json_data=array();  
	    $usd_price= $coin['price_usd'];
	    $usd_cap=$coin['market_cap_usd'];
	 	$usd_vol=$coin['24h_volume_usd'];
		$btc_price= $coin['price_btc'];
	
	    $json_data['btc_price']=format_number($btc_price);
	    $json_data['btc_cap']=cmc_format_coin_values($usd_cap/ $btc_price);
		$json_data['btc_vol']=cmc_format_coin_values($usd_vol/ $btc_price);

		$json_data['usd_price'] = format_number($usd_price);
		$json_data['usd_cap'] = cmc_format_coin_values($usd_cap);
		$json_data['usd_vol'] = cmc_format_coin_values($usd_vol);
		if(is_array($currencies_price_list )){
			foreach ($currencies_price_list as $c_sym=>$currecy) {
			$p_index=strtolower($c_sym).'_price';
			$c_index=strtolower($c_sym).'_cap';
			$v_index=strtolower($c_sym).'_vol';
			$json_data[$p_index]=format_number( $usd_price*$currecy);
			$json_data[$c_index]=cmc_format_coin_values( $usd_cap*$currecy);
			$json_data[$v_index]=cmc_format_coin_values($usd_vol*$currecy);
			}
	  }

		$coin_json=htmlspecialchars(json_encode($json_data),ENT_QUOTES, 'UTF-8');

        $percent_change_1h = $coin['percent_change_1h'] . '%';
        $change_sign ='<i class="fa fa-arrow-up" aria-hidden="true"></i>';
        $change_class = __('up','cmc');
     	$change_sign_minus = "-";
     	
     	
		if($single_page_type==true){$coin_link_open ='<a target="_blank" title="'.$coin_name.'"  href="'.$coin_url.'">';
		$coin_link_close='</a>';
		}
		else{$coin_link_open ='<a title="'.$coin_name.'"  href="'.$coin_url.'">';
		$coin_link_close='</a>';
		}

     	 
 		 $change_sign_24h ='<i class="fa fa-arrow-up" aria-hidden="true"></i>';
          $change_class_24h =  'cmc-up';
         if ( strpos( $coin['percent_change_24h'], $change_sign_minus ) !==false) {
            $change_sign_24h = '<i class="fa fa-arrow-down" aria-hidden="true"></i>';
            $change_class_24h = 'cmc-down';
           
		}
		// small charts colors
		$small_chart_color = '#006400';
		$small_chart_bgcolor = '#90EE90';
		if (strpos($coin['percent_change_7d'], $change_sign_minus) !== false) {
			$small_chart_color = '#ff0000';
			$small_chart_bgcolor = '#ff9999';
		}

		 $changes_coin_html='';
       	 $changes_coin_html.='<span class="ccpw-changes '.$change_class_24h.'">';
			$changes_coin_html.=$change_sign_24h.$percent_change_24h ;
			$changes_coin_html.= '</span>';	

			 $changes_24h_coin_html='';
       	 $changes_24h_coin_html.='<span class="cmc_live_ch cmc-changes '.$change_class_24h.'">';
		 $changes_24h_coin_html.=$change_sign_24h.$percent_change_24h ;
		 $changes_24h_coin_html.= '</span>';	
		$change_sign_1h ='<i class="fa fa-arrow-up" aria-hidden="true"></i>';

        $change_class_1h =  'cmc-up';
         if ( strpos( $coin['percent_change_1h'], $change_sign_minus ) !==false) {
            $change_sign_1h = '<i class="fa fa-arrow-down" aria-hidden="true"></i>';
            $change_class_1h = 'cmc-down';
        }
		
		$changes_1h_coin_html='';
       	$changes_1h_coin_html.='<span class="cmc-changes '.$change_class_1h.'">';
		$changes_1h_coin_html.=$change_sign_1h.$percent_change_1h ;
		$changes_1h_coin_html.= '</span>';		
		$change_sign_7d ='<i class="fa fa-arrow-up" aria-hidden="true"></i>';
        $change_class_7d =  'cmc-up';
        if ( strpos( $coin['percent_change_7d'], $change_sign_minus ) !==false) {
            $change_sign_7d = '<i class="fa fa-arrow-down" aria-hidden="true"></i>';
            $change_class_7d = 'cmc-down';
        }
			
		$changes_7d_coin_html='';
       	$changes_7d_coin_html.='<span class="cmc-changes '.$change_class_7d.'">';
		$changes_7d_coin_html.=$change_sign_7d.$percent_change_7d ;
		$changes_7d_coin_html.= '</span>';	
	  $logo='<span class="cmc_coin_logo">'.$coin_icon.'</span>';
	  $coin_symbol_html='<span class="cmc_coin_symbol">('.$coin_symbol.')</span><br/>';
	  $coin_name_html='<span class="cmc_coin_name cmc-desktop">'.$coin_name.'</span>';	
	  $coin_data.='<tr data-coin-symbol="'.$coin_symbol.'">';
	  $coin_data .='<td>'.$coin['rank'].'</td>';	
	 $coin_data .='<td data-order="'.$coin_name.'"><span class="cmc-name">';
	 $coin_data .=$coin_link_open.$logo.$coin_symbol_html.$coin_name_html.$coin_link_close;
	 $coin_data .='</span></td>';
	
  if($display_price==true){
	$coin_data .='<td data-coin-json="'.$coin_json.'" data-coin-price="'.$coin['price_usd'].'" data-coin-symbol="'.$coin_symbol.'" class="cmc_price_section" data-order="'.$coin_price.'"><span class="cmc-price">' . $currency_icon. $coin_price . '</span></td>';
		}
if($display_changes1h==true){
	$coin_data .='<td data-order="'. $coin['percent_change_1h'] .'">'.$changes_1h_coin_html.'</td>';
	}	
	if($display_changes24h==true){
	$coin_data .= '<td  data-order="'.$coin['percent_change_24h'].'">'.$changes_24h_coin_html.'</td>';	
	}	
	if($display_Changes7d==true){
	$coin_data .='<td  data-order="'.$coin['percent_change_7d'].'">'.$changes_7d_coin_html.'</td>';
	}	

    
	if($display_market_cap==true){
	$coin_data .='<td data-order="'.$market_cap.'" ><span class="cmc_live_cap">'.$currency_icon.$formatted_market_cap.'</span></td>';
		}	
	
	if($display_Volume_24h==true){
	$coin_data .='<td data-order="'.$volume.'" ><span class="cmc_live_vol">'.$currency_icon.$formatted_volume.'</span></td>';
	}
	
	
	if($display_supply==true){
	$coin_data .='<td data-order="'.$supply.'" ><span class="cmc_live_supply">'.$formatted_supply.' '.$coin_symbol.'</span></td>';
	}

	
	 if($display_chart==true){

	if (cmc_isMobileDevice()==0) {
	
	$coin_data .='<td><div class="small-chart-area">';
	if($cmc_small_charts=="image-charts"){
		$coin_data .=$coin_link_open.get_coin_chart($coin_symbol).$coin_link_close;
	}
		else{
		$period = '7d';
		$points = 0;
		$currency_symbol = "$";
		if($old_currency=="USD"){
			$currency_price = 1;
		}else{
			$currency_price = $selected_currency_rate;
		}
		
		$chart_fill = "true";

		if($coin_symbol=="MIOTA"){
			$coin_symbol="IOT";
		}
		$chart_cache=$this->cmc_get_chart_cache_data($coin_symbol);
		$cachedata='';
		$content='';
		if($chart_cache===false){
			$cachedata='false';
		}else{
			$cachedata='true';
			$content=json_encode($chart_cache);
		}
		$no_data_lbl=__('No Graphical Data','cmc');
			$coin_data .= '<div class="cmc_spinner">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
		</div>';
			$coin_data .= '<div style="width:100%;height:100%;"  class=" ccpw-chart-container">
		<canvas data-bg-color="' . $small_chart_bgcolor . '"
			data-color="' . $small_chart_color . '" 
			data-msz="' . $no_data_lbl . '"
			data-content="' . $content . '" 
			data-cache="' . $cachedata . '" 
			data-coin-symbol="' . $coin_symbol . '" 
			data-period="' . $period . '"
			data-points="' . $points . '"
			data-currency-symbol="' . $currency_symbol . '"
			data-currency-price="' . $currency_price . '"
			data-chart-fill="' . $chart_fill . '"
		  class="cmc-sparkline-charts" id="small-coinchart"></canvas>
		  </div>';
		
		}

		$coin_data.='</div></td>'; 
		}
	}
	  $coin_data.='</tr>';

echo $coin_data;