<?php
class CMC_Shortcode
{

	function __construct()
	{
		add_action('wp_enqueue_scripts', array($this, 'cmc_register_scripts'));
		add_shortcode('global-coin-market-cap', array($this, 'cmc_global_data'));
		add_shortcode('coin-market-cap', array($this, 'cmc_shortcode'));
		require_once(CMC_PATH . '/includes/pagination.class.php');
		if (cmc_isMobileDevice() == 0) {
			add_action('wp_ajax_cmc_small_charts', array($this, 'cmc_small_chart_data'));
			add_action('wp_ajax_nopriv_cmc_small_charts', array($this, 'cmc_small_chart_data'));
		}

	}	

	// small chart data handler
	function cmc_small_chart_data()
	{

		if (isset($_POST['symbol'])) {
			$symbol = $_POST['symbol'];
			$history = cmc_histo_7days($symbol);

			if (!empty($history) && count($history) > 0) {
				echo json_encode(array("type" => "success", "data" => $history));
			} else {
				echo json_encode(array("type" => "error", "data" => $history));
			}
		}
		wp_die();
	}

	// grabing data from cache
	function cmc_get_chart_cache_data($coin_symbol)
	{
		$historical_data = get_transient('7d-historical-' . $coin_symbol);

		if (is_array($historical_data) && count($historical_data) > 0) {
			return $historical_data;
		} else {
			return false;
		}

	}

	// main shortcode for coin market cap table. 
	function cmc_shortcode($atts, $content = null)
	{

		$atts = shortcode_atts(array(
			'id' => '',
			'class' => '',
			'info' => true,
			'paging' => false,
			'scrollx' => true,
			'ordering' => true,
			'searching' => false,
		), $atts, 'cmc');
	
	
		wp_enqueue_script('bootstrapcdn');
		wp_enqueue_script('cmc-jquery-number');
		wp_enqueue_script('cmc-datatables.net');
		wp_enqueue_script('cmc-typeahead');
		wp_enqueue_script('cmc-handlebars');
		wp_enqueue_script('cmc-chartjs');
		wp_enqueue_script('cmc-small-charts');
		wp_enqueue_script('cmc-fixed-column-js');
		wp_enqueue_script('cmc-js');

		wp_localize_script(
			'cmc-js',
			'ajax_object',
			array('ajax_url' => admin_url('admin-ajax.php'))
		);

		if (cmc_isMobileDevice() == 0) {
			wp_enqueue_script('ccc-socket', 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js', array('jquery'), false, true);
			wp_enqueue_script('ccc_stream', CMC_URL . 'assets/js/cmc-stream.min.js', array(), null, true);
		}

		$cmc_link_array = array();
		$post_id = $atts['id'];
		// Initialize Titan
		$cmc_titan = TitanFramework::getInstance('cmc_single_settings');
		$show_coins = $cmc_titan->getOption('show_currencies', $post_id);
		$real_currency = $cmc_titan->getOption('old_currency', $post_id);
		$old_currency = $real_currency ? $real_currency : "USD";
		// for currency dropdown
		$currencies_price_list = cmc_usd_conversions('all');
		$selected_currency_rate = cmc_usd_conversions($old_currency);
		$currency_symbol = cmc_old_cur_symbol($old_currency);
		$single_default_currency = $cmc_titan->getOption('default_currency', $post_id);
		$fetch_coins = 1600;
		$pagination = $show_coins ? $show_coins : 50;
		$display_supply = $cmc_titan->getOption('display_supply', $post_id);
		$display_Volume_24h = $cmc_titan->getOption('display_Volume_24h', $post_id);
		$display_market_cap = $cmc_titan->getOption('display_market_cap', $post_id);
		$display_price = $cmc_titan->getOption('display_price', $post_id);
		$display_Changes7d = $cmc_titan->getOption('display_Changes7d', $post_id);

		$display_changes24h = $cmc_titan->getOption('display_changes24h', $post_id);
		$display_changes1h = $cmc_titan->getOption('display_changes1h', $post_id);

		$display_chart = $cmc_titan->getOption('coin_price_chart', $post_id);
		$cmc_small_charts = $cmc_titan->getOption('cmc_chart_type', $post_id);
		$live_updates_cls = '';
		$live_updates = $cmc_titan->getOption('live_updates', $post_id);
		if ($live_updates) {
			$live_updates_cls = 'cmc_live_updates';
		} else {
			$live_updates_cls = '';
		}
		$enable_formatting = $cmc_titan->getOption('enable_formatting', $post_id);


		$single_page_type = $cmc_titan->getOption('single_page_type', $post_id);

		$single_page_slug = cmc_get_page_slug();

		$cmc_data_attributes = '';
		$att['info'] = $this->cmc_data_attribute($atts['info']);
		$att['paging'] = $this->cmc_data_attribute($atts['paging']);
		$att['scrollx'] = $this->cmc_data_attribute($atts['scrollx']);
		$att['ordering'] = $this->cmc_data_attribute($atts['ordering']);
		$att['searching'] = $this->cmc_data_attribute($atts['searching']);

		foreach ($att as $att_key => $att_value) {
			$att_key = esc_attr($att_key);
			$att_value = esc_attr($att_value);
			$cmc_data_attributes .= " data-{$att_key}=\"{$att_value}\" ";
		}

		$cmc_data_attributes .= 'data-pageLength="' . $pagination . '"';
		$cmc_coins_page = (get_query_var('page') ? get_query_var('page') : 1);
		$all_coin_data = cmc_coins_data($fetch_coins, $old_currency);
		$bitcoin_price = isset($all_coin_data[0]->price_usd) ? $all_coin_data[0]->price_usd : 1;

		if (is_array($all_coin_data) && count($all_coin_data) > 0) {
			$pagination = new pagination($all_coin_data, $cmc_coins_page, $pagination);
			$show_coins = $pagination->getResults();
		} else {
			_e('API Request timeout', 'cmc');
		}


		$c_json = currencies_json();
		ob_start();
		$search = __('search', 'cmc');
		?>

		<!---------- CMC Version:- <?php echo CMC; ?> By Cool Plugins Team-------------->
		<div id="cryptocurency-market-cap-wrapper">
		
		<script id="cmc_curr_list" type="application/json">
		 <?php echo $c_json; ?>
		</script>	
	
		<div class="cmc_price_conversion">
			<select id="cmc_usd_conversion_box" class="cmc_conversions">
				<?php 
			$currencies_price_list['BTC'] = $bitcoin_price;

			foreach ($currencies_price_list as $name => $price) {
				$csymbol = cmc_old_cur_symbol($name);
				if ($name == $old_currency) {

					echo '<option selected="selected" data-currency-symbol="' . $csymbol . '" data-currency-rate="' . $price . '"  value="' . $name . '" >' . $name . '</option>';
				} else {
					echo '<option data-currency-symbol="' . $csymbol . '" data-currency-rate="' . $price . '"  value="' . $name . '">' . $name . '</option>';
				}
			}
			unset($currencies_price_list['BTC']);

			?>
			</select>
		</div>

		<div  data-currency="<?php echo $old_currency; ?>" class="cmc_search" id="custom-templates">
  		<input class="typeahead" type="text" placeholder="<?php echo $search ?>">
		</div>
		<?php 
	if (is_array($all_coin_data) && count($all_coin_data) > 0) {

		foreach ($all_coin_data as $coin) {
			$coin = (array)$coin;
			$coin_id = $coin['id'];
			$coin_symbol = $coin['symbol'];
			$name = $coin['name'] . " " . $coin_symbol . "";
			$coin_logo = coin_logo_url($coin_id, $size = 32);
			if ($old_currency == $single_default_currency) {
				$coin_url = $single_page_slug . '/' . $coin_symbol . '/' . $coin_id;
			} else {
				$coin_url = $single_page_slug . '/' . $coin_symbol . '/' . $coin_id . '/' . $old_currency;
			}
			$cmc_link_array[] = array("link" => $coin_url, "name" => $name, "symbol" => $coin_symbol, "logo" => $coin_logo);
		}
		$search_links = json_encode($cmc_link_array);
	}
	?>
		<div class="cmc_pagi">
		<?php
	if (is_array($all_coin_data) && count($all_coin_data) > 0) {
		$pagination->setShowFirstAndLast(true);
		echo $pageNumbers = '<div class="cmc_p_wrp"><ul  class="cmc_pagination">' . $pagination->getLinks($_GET) . '</li></div>';
	}
	?>
	</div>
<div class="top-scroll-wrapper">
    <div class="top-scroll">
    </div>
	</div>
	<table id="cmc_coinslist" data-currency-symbol="<?php echo $currency_symbol; ?>" data-currency-rate="<?php echo $selected_currency_rate; ?>" data-old-currency="<?php echo $old_currency; ?>" class="<?php echo $live_updates_cls; ?> cmc-datatable table table-striped table-bordered" width="100%"   <?php echo $cmc_data_attributes; ?>>
		<thead>
		<tr>
		<th class="desktop"><?php _e('#', 'cmc'); ?></th>
			<th class="all"><?php _e('Name', 'cmc'); ?></th>
		
		<?php if ($display_price == true) { ?>
		<th class="all"><?php _e('Price', 'cmc'); ?></th>
		<?php 
		} ?>

		<?php
		if ($display_changes1h == true) {
		?>	<th><?php
		_e('Changes ', 'cmc') . ' <span class="badge  badge-default">' . _e('1H ', 'cmc') . '</span>';
		?></th>    <?php	}	?>
																																																																			
		<?php
		if ($display_changes24h == true) {
		?><th><?php
		_e('Changes ', 'cmc') . '<span class="badge  badge-default">' . _e('24H', 'cmc') . '</span>';
		?></th><?php } ?>

		<?php
		if ($display_Changes7d == true) {?>
		<th><?php
		_e('Changes ', 'cmc') . '<span class="badge badge-default">' . _e('7D', 'cmc') . '</span>';
		?></th>   
		 <?php } ?>

		<?php
		if ($display_market_cap == true) {
		?>
		<th><?php
		_e('Market Cap', 'cmc');
		?></th>    <?php

		}
		?>

		<?php
		if ($display_Volume_24h == true) {
		?>
		<th><?php
		_e('Volume ', 'cmc') . '<span class="badge  badge-default">' . _e('24H', 'cmc') . '</span>';
		?></th>    <?php

		}
		?>

		<?php
		if ($display_supply == true) {
		?>
		<th><?php
		_e('Available Supply', 'cmc');
		?></th>    <?php

		}
		?>

		<?php
		if ($display_chart == true) {

		if (cmc_isMobileDevice() == 0) {
		?>
		<th  data-orderable="false"><?php
		_e('Price Graph ', 'cmc') . _e('<span class="badge badge-default">(7D)</span>','cmc2');
		?></th>
		<?php

		}
		}
		?>

		
		</tr>

		</thead>
		<tbody>
		<?php 
	if (is_array($show_coins) && count($show_coins) > 0) {
		foreach ($show_coins as $coin) {
			$coin = (array)$coin;
			$coin_id = $coin['id'];
			$coin_symbol = $coin['symbol'];
			if ($old_currency == $single_default_currency) {
				$coin_url = esc_url(home_url($single_page_slug . '/' . $coin_symbol . '/' . $coin_id . '/', '/'));
			} else {
				$coin_url = esc_url(home_url($single_page_slug . '/' . $coin_symbol . '/' . $coin_id . '/' . $old_currency . '/', '/'));
			}
			// creating HTML for main list 
			require(CMC_PATH . '/includes/cmc-generate-html.php');

		}
	}

	?></tbody>
				<tfoot>
				
				</tfoot>
			</table>
		</div>
		<script id="cmc_search_html" type="application/json">
		<?php echo $search_links; ?>
		</script>
		<script id="search_temp" type="text/x-handlebars-template">
	 <div class="cmc-search-sugestions"><a href="<?php echo esc_url(home_url('/')); ?>{{link}}">
	 	 {{#if logo.local}}
	 	<img src="<?php echo CMC_URL; ?>assets/coins-logos/{{logo.logo}}">
	 	 {{else}}
	 	 <img src="{{logo.logo}}">
	 	{{/if}} 
	 {{name}}</a></div>
		</script>

		<?php
	if (is_array($all_coin_data) && count($all_coin_data) > 0) {
		echo $pageNumbers;
	}
	?>
		<?php return ob_get_clean();

}



function cmc_get_settings($post_id, $index)
{
	if ($post_id && $index) {
			// Initialize Titan
		$cmc_titan = TitanFramework::getInstance('cmc_single_settings');

		$val = $cmc_titan->getOption($index, $post_id);
		if ($val) {
			return true;
		} else {
			return false;
		}
	}
}

function cmc_data_attribute($attr)
{
	$coin_html = '';
	if ('true' === $attr || true === $attr || 1 == $attr) {
		return 'true';
	} else {
		return 'false';
	}
}


/*
 Global Info shortcode handler
 */
function cmc_global_data($atts, $content = null)
{

	$atts = shortcode_atts(array(
		'id' => '',
		'currency' => 'USD',
		'formatted' => true
	), $atts);

	wp_register_style('cmc-global-style', false);
	wp_enqueue_style('cmc-global-style');

	$cmc_g_styles = '/* Global Market Cap Data */
		.cmc_global_data {
			display:inline-block;
			margin-bottom:5px;
			width:100%;
		}
		.cmc_global_data ul {
		    list-style: none;
		    margin: 0;
		    padding: 0;
		    display: inline-block;
		    width: 100%;
		}
		.cmc_global_data ul li {
		    display: inline-block;
		    margin-right: 20px;
			font-size:14px;
			margin-bottom: 5px;
		}
		.cmc_global_data ul li .global_d_lbl {
			font-weight: bold;
		    background: #f9f9f9;
		    padding: 4px;
		    color: #3c3c3c;
		    border: 1px solid #e7e7e7;
		    margin-right: 5px;
		}
		.cmc_global_data ul li .global_data {
		    font-size: 13px;
			white-space:nowrap;
			display:inline-block;
		}
		/* Global Market Cap Data END */ ';

	wp_add_inline_style('cmc-global-style', $cmc_g_styles);


	$output = '';
	$old_currency = $atts['currency'] ? $atts['currency'] : 'USD';

	$currency_symbol = cmc_old_cur_symbol($old_currency);
	$market_cap_index = "total_market_cap_" . strtolower($old_currency);
	$volume_index = "total_24h_volume_" . strtolower($old_currency);
	$global_data = (array)cmc_get_global_data($old_currency);

	if (is_array($global_data)) {
		$bitcoin_percentage_of_market_cap = $global_data['bitcoin_percentage_of_market_cap'];

		$output .= '<div class="cmc_global_data"><ul>';


		if (isset($global_data[$market_cap_index])) {

			if ($atts['formatted'] == "true") {
				$mci_html = $currency_symbol . cmc_format_coin_values($global_data[$market_cap_index]);
			} else {
				$mci_html = $currency_symbol . format_number($global_data[$market_cap_index]);
			}
			$output .= '<li><span class="global_d_lbl">' . __('Market Cap:', 'cmc') . '</span><span class="global_data"> ' . $mci_html . '</span></li>';
		}

		if (isset($global_data[$volume_index])) {
			if ($atts['formatted'] == "true") {
				$vci_html = $currency_symbol . cmc_format_coin_values($global_data[$volume_index]);
			} else {
				$vci_html = $currency_symbol . format_number($global_data[$volume_index]);
			}
			$output .= '<li><span class="global_d_lbl">' . __('24h Vol:', 'cmc') . '</span><span class="global_data"> ' . $vci_html . '</span></li>';
		}
		$output .= '<li><span class="global_d_lbl">' . __('BTC Dominance: ', 'cmc') . '</span><span class="global_data">' . $bitcoin_percentage_of_market_cap . '%</span></li>';

		$output .= '</ul></div>';
	}
	return $output;
}




/**
 * Register scripts and styles
 */
	 //add cdn and change js file functions
public function cmc_register_scripts()
{
	if (!is_admin()) {

		if (!wp_script_is('jquery', 'done')) {
			wp_enqueue_script('jquery');
		}
		wp_enqueue_style('cmc-font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

		wp_register_script('cmc-datatables.net', 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js');
		wp_register_script('bootstrapcdn', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
            
		wp_register_style('cmc-custom', CMC_URL . 'assets/css/cmc-custom.min.css');
		wp_register_style('cmc-bootstrap', CMC_URL . 'assets/css/bootstrap.min.css');

		wp_register_script('cmc-jquery-number', 'https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js');

		wp_register_script('cmc-js', CMC_URL . 'assets/js/cmc-main.min.js', array('jquery', 'cmc-datatables.net'), false, true);

		wp_register_script('cmc-fixed-column-js', 'https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js', array('jquery'), false, true);

		wp_register_script('cmc-typeahead', CMC_URL . 'assets/js/typeahead.bundle.min.js', array('jquery'), false, true);
		wp_register_script('cmc-handlebars', CMC_URL . 'assets/js/handlebars-v4.0.11.js', array('jquery'), false, true);
		wp_register_script('cmc-chartjs', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js');
		wp_register_script('cmc-small-charts', CMC_URL . 'assets/js/small-charts.js', array('jquery', 'cmc-chartjs'), false, true);
		wp_localize_script(
			'cmc-js',
			'ajax_object',
			array('ajax_url' => admin_url('admin-ajax.php'))
		);
		$dynamic_css = cmc_dynamic_style();
		wp_add_inline_style('cmc-custom', $dynamic_css);

		//loading globally for fast rendering
		wp_enqueue_style('cmc-bootstrap');
		wp_enqueue_style('cmc-custom');
	}
}


}